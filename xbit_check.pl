#-------------------------------------------------------------------------------
#
# $perl xbit_info.pl V:\PROJECT\top_video.bit
#
# http://www.fpga-faq.com/FAQ_Pages/0026_Tell_me_about_bit_files.htm
#
#-------------------------------------------------------------------------------
use strict;

my $file = $ARGV[0];
my $buff;

#!-----------------------------------------------------------------------------
#! HEADER
#!-----------------------------------------------------------------------------
open(FILE, "<", $file) or die $!;
binmode FILE;
read(FILE, $buff, 0x10);	#! skip
read(FILE, $buff, 0x90);
my @str = split(m/[\x00-\x1F\x80-\xFF;]+/, unpack("a*",$buff));
#print join("|", @str)."\n";
print "-------------------------------\n";
print "NCD file  : $str[0]\n";
print "            $str[1]\n";
print "            $str[2]\n";
print "Part name : $str[4]\n";
print "Creation  : $str[6] $str[8]\n";
print "-------------------------------\n";

#!-----------------------------------------------------------------------------
#! http://nahitafu.cocolog-nifty.com/nahitafu/2010/08/sp6mitoujtag-ae.html
#!-----------------------------------------------------------------------------
open(FILE, "<", $file) or die $!;
binmode FILE;
read(FILE, $buff, 0xA3);	#! skip
read(FILE, $buff, 0x04);	#! 4byte, 0x2BA114DD, 2016/07/05 17:19:29
my $hex = unpack("H*",$buff);
printf "sync-word : 0x\U$hex\L (%s)\n", ($hex eq "aa995566") ? "ok" : "NG";
print "-------------------------------\n";

#!-----------------------------------------------------------------------------
#! TIMESTAMP
#! j_xapp497_usr_access.pdf
#!-----------------------------------------------------------------------------
open(FILE, "<", $file) or die $!;
binmode FILE;
read(FILE, $buff, 0xEF);	#! skip
read(FILE, $buff, 0x04);	#! 4byte, 0x2BA114DD, 2016/07/05 17:19:29
my $hex = unpack("H*",$buff);
my $bit = unpack("B*",$buff);
my @bit = $bit =~ m/(.{5})(.{4})(.{6})(.{5})(.{6})(.{6})/;
foreach(@bit){ $_ = eval "0b$_"; }

printf "USR_ACCESS: 0x\U$hex\n";
printf "TIMESTAMP : %04d/%02d/%02d %02d:%02d:%02d\n",
	$bit[2] + 2000, $bit[1], $bit[0], $bit[3], $bit[4], $bit[5];
print "-------------------------------\n";

__END__

$dump -s256 V:\AH4801J_K7___120Hz_249MHz\video_k7_top.bit
00000000  00 09 0F F0 0F F0 0F F0-0F F0 00 00 01 61 00 34 |.............a.4
00000010  76 69 64 65 6F 5F 6B 37-5F 74 6F 70 2E 6E 63 64 |video_k7_top.ncd
00000020  3B 48 57 5F 54 49 4D 45-4F 55 54 3D 46 41 4C 53 |;HW_TIMEOUT=FALS
00000030  45 3B 55 73 65 72 49 44-3D 30 78 46 46 46 46 46 |E;UserID=0xFFFFF
00000040  46 46 46 00 62 00 0D 37-6B 33 32 35 74 66 66 67 |FFF.b..7k325tffg
00000050  39 30 30 00 63 00 0B 32-30 31 34 2F 30 37 2F 30 |900.c..2014/07/0
00000060  38 00 64 00 09 31 30 3A-35 37 3A 35 34 00 65 00 |8.d..10:57:54.e.
00000070  AE 9D 9C FF FF FF FF FF-FF FF FF FF FF FF FF FF |ｮ撩.............
00000080  FF FF FF FF FF FF FF FF-FF FF FF FF FF FF FF FF |................
00000090  FF FF FF 00 00 00 BB 11-22 00 44 FF FF FF FF FF |......ｻ.".D.....
000000A0  FF FF FF AA 99 55 66 20-00 00 00 30 02 20 01 00 |...ｪ儷f ...0. ..
000000B0  00 00 00 30 02 00 01 00-00 00 00 30 00 80 01 00 |...0.......0.�..
000000C0  00 00 00 20 00 00 00 30-00 80 01 00 00 00 07 20 |... ...0.�.....
000000D0  00 00 00 20 00 00 00 30-02 60 01 00 00 00 00 30 |... ...0.`.....0
000000E0  01 20 01 02 00 3F E5 30-01 C0 01 00 00 00 00 30 |. ...?.0.ﾀ.....0
000000F0  01 80 01 03 65 10 93 30-00 80 01 00 00 00 09 20 |.�..e..0.�.....

$

11101 0011 001011 10101 110101 000011
Where:
5 bits for day: 11101 = 29th day
4 bits for month: 0011 = 3rd month
6 bits for year: 001011 = 11th year (2011)
5 bits for hour: 10101 = 21 
6 bits for minute: 110101 = 53
6 bits for seconds: 000011 = 3
