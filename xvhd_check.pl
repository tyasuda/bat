use strict;
use Regexp::Assemble;

my %word;
my %save;

while(<>){
	s/--.*//;
	s/:=.+//;

	if(m/attribute\s+s\s+of\s+([^:]+?)\s*:/){
		my $signal = $1;
		$save{$signal} = $.;
	}

	next if m/^\s*attribute\b/i;

	if(m/^\s*(signal|constant|variable)\s+([^:]+?)\s*:/i){	#! signal 宣言部
		my($type, $words) = ($1, $2);

		foreach my $word (split(m/\W+/, $words)){
#			print "$.\t$word\t$type\n";
			$word{$word} = 1;
		}
	}else{
		foreach my $word (split m/\W+/){
			if(exists $word{$word}){
				$word{$word}++;
			}
		}
	}
}

my @word1;
my @word2;

while(my($word, $freq) = each(%word)){
	next if $word =~ m/^\d/;
	next if $word =~ m/^unused\d*$/;
	next if $word =~ m/^vector\d+_array$/;
	next if $word =~ m/^\b(?:(?:(?:S(?:T(?:D_LOGIC(?:_VECTOR)?|RING)|E(?:VERITY(?:_LEVEL)?|LECT)|UBTYPE|HARED|IGNAL|L[AL]|R[AL])|R(?:E(?:A(?:D(?:LINE)?|L)|(?:JEC|POR)T|GISTER|CORD|TURN|M)|O[LR]|ANGE|IGHT)|A(?:R(?:CHITECTURE|RAY)|(?:CCES|B)S|L(?:IAS|L)|TTRIBUTE|SSERT|FTER|ND)|P(?:O(?:S(?:ITIVE|PONED)|RT)|ROCE(?:DURE|SS)|(?:ACKAG|UR)E)|E(?:N(?:D(?:(?:FIL|LIN)E)?|TITY)|(?:VEN|XI)T|LS(?:IF|E))|B(?:IT(?:_VECTOR)?|O(?:OLEAN|DY)|U(?:FFER|S)|EGIN|LOCK)|C(?:O(?:N(?:FIGURATION|STANT)|MPONENT)|HARACTER|ASE)|L(?:I(?:N(?:KAG)?E|BRARY|TERAL)|ABEL|EFT|OOP)|I(?:[FS]|N(?:ERTIAL|[OP]UT|TEGER)?|MPURE)|T(?:(?:IM|YP)E|EXT(?:IO)?|RANSPORT|HEN|O)|W(?:RIT(?:ELIN)?E|H(?:ILE|EN)|ID?TH|AIT)|N(?:A(?:TURAL|ND)|E(?:XT|W)|O[RTW]|ULL)|O(?:[FR]|(?:UTP)?UT|(?:PE)?N|THERS)|G(?:ENER(?:ATE|IC)|UARDED|ROUP)|U(?:N(?:AFFECTED|ITS|TIL)|SE)|D(?:ISCONNECT|OWNTO)|F(?:UNCTION|ILE|OR)|M(?:AP|OD)|VARIABLE|XN?OR)))\b/i;

	if($freq == 1){
		push(@word1, $word);
	}

	if($freq == 2 and not exists $save{$word}){
		push(@word2, $word);
	}
}

if(@word1){
	print STDERR "--- unused signal(s) found ---\n";
	print STDERR join("\n", @word1)."\n";

	my $ro = Regexp::Assemble->new;
	$ro->add(@word1);
	my $re = "\\b(?:".$ro->re.")\\b";;
	print $re, "\r\n";	# tocli でクリップボードにコピーするときは \r が要る。
}

if(@word1 == 0 and @word2){
	print STDERR "--- dangling signal(s) found ---\n";
	print STDERR join("\n", @word2)."\n";

	my $ro = Regexp::Assemble->new;
	$ro->add(@word2);
	my $re = "\\b(?:".$ro->re.")\\b";;
	print $re, "\r\n";	# tocli でクリップボードにコピーするときは \r が要る。
}


if(@word1 == 0 and @word2 == 0){
	print STDERR "ok\n";
}

__END__
unused
ABS
ACCESS
AFTER
ALIAS
ALL
AND
ARCHITECTURE
ARRAY
ASSERT
ATTRIBUTE
BEGIN
BIT
BIT_VECTOR
BLOCK
BODY
BOOLEAN
BUFFER
BUS
CASE
CHARACTER
COMPONENT
CONFIGURATION
CONSTANT
DISCONNECT
DOWNTO
ELSE
ELSIF
END
ENDFILE
ENDLINE
ENTITY
EVENT
EXIT
FILE
FOR
FUNCTION
GENERATE
GENERIC
GROUP
GUARDED
IF
IMPURE
IN
INERTIAL
INOUT
INPUT
INTEGER
IS
LABEL
LEFT
LIBRARY
LINE
LINKAGE
LITERAL
LOOP
MAP
MOD
NAND
NATURAL
NEW
NEXT
NOR
NOT
NOW
NULL
OF
ON
OPEN
OR
OTHERS
OUT
OUTPUT
PACKAGE
PORT
POSITIVE
POSPONED
PROCEDURE
PROCESS
PURE
RANGE
READ
READLINE
REAL
RECORD
REGISTER
REJECT
REM
REPORT
RETURN
RIGHT
ROL
ROR
SELECT
SEVERITY
SEVERITY_LEVEL
SHARED
SIGNAL
SLA
SLL
SRA
SRL
STD_LOGIC
STD_LOGIC_VECTOR
STRING
SUBTYPE
TEXT
TEXTIO
THEN
TIME
TO
TRANSPORT
TYPE
UNAFFECTED
UNITS
UNTIL
USE
VARIABLE
WAIT
WHEN
WHILE
WIDTH
WITH
WRITE
WRITELINE
XNOR
XOR
